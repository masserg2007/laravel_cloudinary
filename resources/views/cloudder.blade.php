<!DOCTYPE html>
<html>
<head>
    <title>Laravel 5 cloudinary upload file and images with example</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
</head>
<body>
 @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
    </div>
    <img src="{{ Session::get('image') }}">
@endif
{!! Form::open(array('route' => 'upload-file','files'=>true)) !!}
            <div class="row">
                <div class="col-md-6">
                    {!! Form::file('image_file', array('class' => 'form-control')) !!}
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
            </div>
{!! Form::close() !!}
</body>
</html>