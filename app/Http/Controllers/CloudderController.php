<?php

// https://github.com/jrm2k6/cloudder
// http://www.expertphp.in/article/laravel-5-cloudinary-upload-file-and-images-with-example

// https://res.cloudinary.com/masserg/image/upload/v1559219369/ldsglyemkx2lplhiouk0.jpg
// https://res.cloudinary.com/masserg/image/upload/v1559220127/gvdo1s9omr8wpgyf7nb4.jpg

namespace App\Http\Controllers;
use Illuminate\Http\Request;
class CloudderController extends Controller
{
    public function getFile(){
        echo "<img src='".\Cloudder::show('gvdo1s9omr8wpgyf7nb4', array())."'>";
        return view('cloudder');
    }
    public function uploadFile(Request $request){
          if($request->hasFile('image_file')){
            \Cloudder::upload($request->file('image_file'));
            $id = \Cloudder::getPublicId();
            $c = \Cloudder::getResult();
//            Cloudder::destroyImage($publicId, array $options)
//            Cloudder::delete($publicId, array $options)
            if($c){
               return back()
                    ->with('success','You have successfully upload images. -> '.$id)
                    ->with('image',$c['url']);
            }
        }
    }
}